#!/bin/sh
set -x

# Two jobs cannot acquire the same swtpm which leads to failure of both jobs
# Hence if there is already running, then other jobs which needs swtpm must wait.
# Once the job which acquired swtpm is done, the process will be gone.
while [ "$(pstree -p | grep swtpm)" ]; do
    echo "TPM socket acquired by another job, please wait..."
    sleep 30s
done

# Create a temporary directory to store swtpm state files
mkdir -p /var/lib/lava/dispatcher/tmp/cip-core-security.swtpm

# Start the swtpm dameon process
if swtpm socket -t -d --tpmstate dir=/var/lib/lava/dispatcher/tmp/cip-core-security.swtpm \
                         --ctrl type=unixio,path=/tmp/qemu-swtpm.sock \
                         --tpm2; then

	echo "swtpm socket creation success"
	exit 0
else
	echo "swtpm socket creation failure"
	exit 1
fi
